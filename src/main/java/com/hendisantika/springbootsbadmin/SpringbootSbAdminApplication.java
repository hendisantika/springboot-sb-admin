package com.hendisantika.springbootsbadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSbAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSbAdminApplication.class, args);
    }
}
